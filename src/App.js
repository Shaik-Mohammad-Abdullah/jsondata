import React, { Component } from "react";
import { Route, Routes } from "react-router-dom";
import PostRouterFile from "./components/PostRouterFile";
import UserRouterFile from "./components/UserRouterFile";
import Posts from "./components/Posts";

export default class App extends Component {
    render() {
        return (
            <div className="App">
                <Routes>
                    <Route exact path="/" element={<Posts />} />
                    <Route
                        exact
                        path="/post/:postId"
                        element={<PostRouterFile />}
                    />
                    <Route
                        exact
                        path="/user/:userId"
                        element={<UserRouterFile />}
                    />
                    <Route element={Error} />
                </Routes>
            </div>
        );
    }
}
