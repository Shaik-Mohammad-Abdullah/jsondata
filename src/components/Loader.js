import React, { Component } from "react";

export default class Loader extends Component {
    render() {
        return (
            <div className="loader">
                <div className="loading">Kindly wait... Data is loading...</div>
            </div>
        );
    }
}
