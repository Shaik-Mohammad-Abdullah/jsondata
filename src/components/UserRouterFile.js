import React, { Component } from "react";
import userImage from "./images/blank-profile-picture-973460__480.png";
import Loader from "./Loader";
import ErrorUser from "./ErrorUser";

export default class Users extends Component {
    constructor() {
        super();
        this.state = {
            userData: null,
            isDataLoaded: false,
            error: false,
        };
    }

    setErrorState = () => (this.error = true);

    data = async () => {
        const path = window.location.pathname;
        const index = path.lastIndexOf("/");
        const data = path.slice(index + 1);
        if (data < 0 || data > 10 || isNaN(data) || data[0] === "0") {
            this.setErrorState();
            return;
        }

        try {
            const userBuffer = await fetch(
                "https://jsonplaceholder.typicode.com/users/" + data
            );
            const userData = await userBuffer.json();
            this.setState({
                userData: userData,
                isDataLoaded: true,
            });
        } catch (error) {
            console.error(error);
        }
    };

    componentDidMount = () => {
        this.data();
    };

    render() {
        const { isDataLoaded, userData, error } = this.state;
        console.log(userData);

        if (error) {
            return <ErrorUser />;
        }

        if (!isDataLoaded) {
            return <Loader />;
        }

        return (
            <div className="userData">
                <h1>Information of User with ID: {userData.id}</h1>
                <div className="user">
                    <img
                        src={userImage}
                        alt={userData.name}
                        className="image"
                        title={userData.username}
                    />
                    <div className="userProfile">
                        <p className="user-para">
                            <span>Name: </span>
                            {userData.name}
                        </p>
                        <p className="user-para">
                            <span>Username: </span>@{userData.username}
                        </p>
                        <p className="user-para">
                            <span>Email: </span>
                            {userData.email}
                        </p>
                        <p className="user-para">
                            <span>City: </span>
                            {userData.address.city}
                        </p>
                        <p className="user-para">
                            <span>Contact Number: </span>
                            {userData.phone}
                        </p>
                        <p className="user-para">
                            <span>Website: </span>
                            {userData.website}
                        </p>
                        <p className="user-para">
                            <span>Company Name: </span>
                            {userData.company.name}
                        </p>
                    </div>
                </div>
            </div>
        );
    }
}
