import React, { Component } from "react";

export default class Error extends Component {
    render() {
        return (
            <div className="error-page">
                <h1>Oops! Page not found!</h1>
                <ul className="list-errors">
                    <li>1. Please update the link with the id of the Post</li>
                    <li>2. Check your internet connection</li>
                </ul>
            </div>
        );
    }
}
