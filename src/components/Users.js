import React, { Component } from "react";
import { Link } from "react-router-dom";
import userImage from "./images/blank-profile-picture-973460__480.png";
import Loader from "./Loader";

export default class Users extends Component {
    constructor() {
        super();
        this.state = {
            usersData: [],
            isDataLoaded: false,
        };
    }

    data = async () => {
        try {
            const usersBuffer = await fetch(
                "https://jsonplaceholder.typicode.com/users"
            );
            const usersData = await usersBuffer.json();
            this.setState({
                usersData: usersData,
                isDataLoaded: true,
            });
        } catch (error) {
            console.error(error);
        }
    };

    componentDidMount = () => {
        this.data();
    };

    render() {
        const { isDataLoaded, usersData } = this.state;

        if (!isDataLoaded) {
            return <Loader />;
        }
        return (
            <div className="name">
                <h1>{usersData.name}</h1>
                {usersData.map((user) => {
                    if (user.id === this.props.id) {
                        return (
                            <div key={user.id} className="user">
                                <img
                                    src={userImage}
                                    alt={user.name}
                                    className="image"
                                    title={user.username}
                                />
                                <Link
                                    to={"/user/" + user.id}
                                    className="user-link"
                                >
                                    <div className="userProfile">
                                        <p>Name: {user.name}</p>
                                        <p>Connect me @{user.username}</p>
                                    </div>
                                </Link>
                            </div>
                        );
                    }
                    <Loader />;
                })}
            </div>
        );
    }
}
