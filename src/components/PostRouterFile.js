import React, { Component } from "react";
import "../index.css";
import ErrorPost from "./ErrorPost";
import Users from "./Users";
import Comments from "./Comments";
import Loader from "./Loader";

export class Posts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            postData: [],
            isDataLoaded: false,
            error: false,
        };
    }

    setErrorState = () => this.setState({ error: true });

    data = async () => {
        const path = window.location.pathname;
        const index = path.lastIndexOf("/");
        const data = path.slice(index + 1);
        if (data < 0 || data > 100 || isNaN(data)) {
            this.setErrorState();
            return;
        }

        try {
            const postBuffer = await fetch(
                "https://jsonplaceholder.typicode.com/posts/" + data
            );
            const postData = await postBuffer.json();
            this.setState({
                postData: postData,
                isDataLoaded: true,
            });
        } catch (error) {
            console.error(error);
        }
    };

    componentDidMount = () => {
        this.data();
    };

    render() {
        const { isDataLoaded, postData, error } = this.state;

        if (error) {
            return <ErrorPost />;
        }

        if (!isDataLoaded) {
            return <Loader />;
        }

        return (
            <div className="header">
                <h1 className="super-heading">Post of id: {postData.id}</h1>
                <div className="post">
                    <Users id={postData.userId} />
                    <h4 className="post-title">{postData.title}</h4>
                    <div className="body">{postData.body}</div>
                    <Comments id={postData.id} />
                </div>
            </div>
        );
    }
}

export default Posts;
