import React, { Component } from "react";
import "../index.css";
import Users from "./Users";
import Comments from "./Comments";
import Loader from "./Loader";
import { Link } from "react-router-dom";

export class Posts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            postsData: [],
            isDataLoaded: false,
            commentsArray: Array(101).fill(false),
        };
    }

    data = async () => {
        try {
            const postsBuffer = await fetch(
                "https://jsonplaceholder.typicode.com/posts"
            );
            const postsData = await postsBuffer.json();

            this.setState({
                postsData: postsData,
                isDataLoaded: true,
            });
        } catch (error) {
            console.error(error);
        }
    };

    componentDidMount = () => {
        this.data();
    };

    renderComment(id) {
        const temporaryArray = this.state.commentsArray;
        temporaryArray[id] = !temporaryArray[id];
        this.setState({ commentsArray: temporaryArray });
    }

    render() {
        const { isDataLoaded, postsData, commentsArray } = this.state;

        if (!isDataLoaded) {
            return <Loader />;
        }
        return (
            <div className="header">
                <h1 className="super-heading">The Beginnings of NotFacebook</h1>
                {postsData.map((post) => (
                    <div className="post">
                        <Users id={post.userId} />
                        <Link to={"/post/" + post.id} className="post-link">
                            <h4 className="post-title">{post.title}</h4>
                        </Link>
                        <Link to={"/post/" + post.id} className="post-link">
                            <div className="body">{post.body}</div>
                        </Link>

                        <button
                            className="comment-button"
                            onClick={() => {
                                this.renderComment(post.id);
                            }}
                        >
                            Comments
                        </button>
                        {commentsArray[post.id] ? (
                            <Comments id={post.id} />
                        ) : (
                            <Loader />
                        )}
                    </div>
                ))}
            </div>
        );
    }
}

export default Posts;
