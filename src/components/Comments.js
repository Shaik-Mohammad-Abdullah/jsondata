import React, { Component } from "react";
import Loader from "./Loader";

class Comments extends Component {
    constructor(props) {
        super(props);

        this.state = {
            commentsData: [],
            isDataLoaded: false,
        };
    }

    data = async () => {
        try {
            const commentsBuffer = await fetch(
                "https://jsonplaceholder.typicode.com/comments"
            );
            const commentsData = await commentsBuffer.json();
            this.setState({
                commentsData: commentsData,
                isDataLoaded: true,
            });
        } catch (error) {
            console.error(error);
        }
    };

    componentDidMount = () => {
        this.data();
    };

    render() {
        const { isDataLoaded, commentsData } = this.state;

        if (!isDataLoaded) {
            return <Loader />;
        }
        return (
            <div className="header">
                <h1 className="comment-heading">Comments</h1>
                {commentsData.map((comment) => {
                    if (comment.postId === this.props.id) {
                        return (
                            <div className="comment">
                                <h4 className="comment-name">{comment.name}</h4>
                                <div className="body">{comment.body}</div>
                            </div>
                        );
                    }
                })}
            </div>
        );
    }
}

export default Comments;
